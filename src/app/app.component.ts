import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'random-saving';
  rand = Math.random();
  value: number = 0;
  state : boolean = false;
  ngOnInit(): void {
    // this.EQCSS.apply()
  }

  random(){
    this.value = Math.floor(Math.random() * 100);
    this.state = true;
    console.log(this.value)
  }

  close(){
    this.state = false;
  }
 
}
